package in.openbi.jpa.tenantconfig;

import in.openbi.jpa.enums.TenantTypes;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

@Component
public class TenantSchemaResolver implements CurrentTenantIdentifierResolver {


    @Override
    public String resolveCurrentTenantIdentifier() {
        String t = TenantContext.getCurrentTenant();
        if (t != null) {
            return t;
        } else {
            return TenantTypes.PUBLIC.toString();
        }
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}
