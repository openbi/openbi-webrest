package in.openbi.jpa.enums;

public enum PackageTypes {


    TENANT_ENTITY_PACKAGE("in.openbi.jpa.entity");
    private String value;

    PackageTypes(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
