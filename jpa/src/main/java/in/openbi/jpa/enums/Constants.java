package in.openbi.jpa.enums;

public enum Constants {

    DATASOURCE_URL("jdbc:postgresql://%s");
    private final String value;

    Constants(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
