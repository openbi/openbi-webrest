package in.openbi.jpa.properties;

import in.openbi.jpa.enums.Constants;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotNull;

import static in.openbi.util.Utility.notEmpty;

@ConfigurationProperties(
        prefix = "openbi.datasource"
)
@Getter
@Setter
public class DSProperties {

    private boolean continueOnError;
    @NonNull
    @NotNull(message = "Username is required.")
    private String username;
    private String password;
    @NonNull
    @NotNull(message = "Driver class name is required.")
    private String driverClassName;
    @NonNull
    @NotNull(message = "Database name is required.")
    private String dataDatabase;
    @NonNull
    @NotNull(message = "Default schema name is required.")
    private String dataSchema;
    private String dataParams;
    @NonNull
    @NotNull(message = "Host name is required.")
    private String dataHost;
    private String url;
    @NonNull
    @NotNull(message = "Host port is required.")
    private String dataPort;

    public String getUrl() {

        StringBuilder sb = new StringBuilder();

        if (notEmpty(dataHost)) {
            sb.append(dataHost);
        }
        if (notEmpty(dataPort)) {
            sb.append(":").append(dataPort);
        }
        if (notEmpty(dataDatabase)) {
            sb.append("/").append(dataDatabase);
        }
        if (notEmpty(dataParams)) {
            sb.append("?").append(dataParams);
        }
        this.url = String.format(Constants.DATASOURCE_URL.toString(), sb.toString());
        return this.url;
    }
}
