package in.openbi.jpa.jpaconfig;

import in.openbi.jpa.entity.PojoBase_;
import in.openbi.jpa.properties.DSProperties;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.annotation.Validated;

import javax.sql.DataSource;
import javax.validation.Valid;

@Configuration
@Validated
@EnableConfigurationProperties(DSProperties.class)
public class DataSourceConfig {

    private static final Log log = LogFactory.getLog(DataSourceConfig.class);

    public DataSourceConfig() {
        log.info("Initialized");
    }

    @Bean
    @Primary
//    @ConfigurationProperties(prefix = "openbi.datasource")
    public DataSource generateLocalDataSource(@Valid DSProperties dsProperties) {
        DataSourceProperties dataSourceProperties = new DataSourceProperties();
        dataSourceProperties.setUsername(dsProperties.getUsername());
        dataSourceProperties.setPassword(dsProperties.getPassword());
        dataSourceProperties.setUrl(dsProperties.getUrl());
        dataSourceProperties.setDriverClassName(dsProperties.getDriverClassName());
        DataSource dataSource = dataSourceProperties.initializeDataSourceBuilder().build();
        log.info("Datasource is initialized");
        return dataSource;
    }
}
