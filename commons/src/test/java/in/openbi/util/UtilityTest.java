package in.openbi.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.stream.Stream;

class UtilityTest {

    static Stream<String> blankStrings() {
        return Stream.of("", "   ", null);
    }

    @Test
    void instantiateUtilityTest() {
        InvocationTargetException thrown = Assertions.assertThrows(
                InvocationTargetException.class,
                () -> {
                    Constructor<Utility> constructor = Utility.class.getDeclaredConstructor();
                    Assertions.assertTrue(Modifier.isPrivate(constructor.getModifiers()));
                    constructor.setAccessible(true);
                    constructor.newInstance();
                },
                "Expected instantiation to throw, but it didn't"
        );
        Assertions.assertTrue(thrown.getCause() instanceof IllegalAccessException);
    }

    @ParameterizedTest
    @MethodSource("blankStrings")
    void validateNotEmptyForString(final String str) {
        Assertions.assertFalse(Utility.notEmpty(str));
    }

    @Test
    void valdiateNotEmpty(){
        Assertions.assertTrue(Utility.notEmpty("String"));
    }
}
