package in.openbi.util;

public final class Utility {

    private Utility() throws IllegalAccessException {
        throw new IllegalAccessException("Can't instantiate Utility class.");
    }

    public static boolean notEmpty(String s){
        return s!=null && !s.trim().isEmpty();
    }
}
