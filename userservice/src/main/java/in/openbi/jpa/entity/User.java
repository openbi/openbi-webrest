package in.openbi.jpa.entity;

import in.openbi.jpa.enums.UserAccountStatus;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Entity(name="User")
@Table(name="master_user")
public class User extends PojoBase implements Serializable {
    private static final long serialVersionUID = 3119636615523445463L;
    private String username;
    private String email;
    private String password;
    private UserAccountStatus status ;

}