package in.openbi.jpa.enums;

import java.io.Serializable;

public enum UserAccountStatus implements Serializable {

    ACTIVE, // Account is active.
    SUSPENDED, // Account is suspended due to malicious activity for some time in days.
    LOCKED, // Account is locked due to login attempts with invalid credentials exceeds the limit.
    DE_ACTIVE, // Admin has de-activated the account.
    DELETED, // Admin has deleted the account.
    INACTIVE, // Admin has created user account but the activation date is not yet passed.
    TERMINATED // Account has exceeded the termination date.
}
